<html>
    <head>
        <title>Riset API - Operstox</title>
        <link rel="stylesheet" href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css">
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>

        <script src="https://unpkg.com/@coreui/coreui/dist/js/coreui.bundle.min.js"></script>
    </body>
</html>
