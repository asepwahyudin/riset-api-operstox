@extends('layouts.app', ['class' => 'bg-default'])

@section('content')

    <h1 class="text-center mt-5">Riset API - Marketplace</h1>
    <div class="row mt-5">
        <div class="col-12 col-md-3">
            <a href="{{route('tokopedia')}}"><button type="button" class="btn btn-success btn-lg" style="width:100%;margin-top:10px">Tokopedia</button></a>
        </div>
        <div class="col-12 col-md-3">
            <button type="button" class="btn btn-warning btn-lg" style="width:100%;margin-top:10px">Shopee</button>
        </div>
        <div class="col-12 col-md-3">
            <a href="{{route('bukalapak')}}"><button type="button" class="btn btn-danger btn-lg" style="width:100%;margin-top:10px">Bukalapak</button></a>
        </div>
        <div class="col-12 col-md-3">
            <a href="{{route('lazada')}}"><button type="button" class="btn btn-dark btn-lg" style="width:100%;margin-top:10px">Lazada</button></a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-md-3 offset-md-3">
        <a href="{{route('blibli')}}"><button type="button" class="btn btn-info btn-lg" style="width:100%;margin-top:10px">Blibli</button></a>
        </div>
        <div class="col-12 col-md-3">
            <button type="button" class="btn btn-danger btn-lg" style="width:100%;margin-top:10px">JD.ID</button>
        </div>
    </div>

@endsection
