@extends('layouts.app', ['class' => 'bg-default'])

@section('content')

    <h1 class="text-center mt-5">Data Product - Tokopedia</h1>
    <div class="row mt-5">
        <div class="col-12 col-md-12">
            <a href="{{route('/')}}"><button type="button" class="btn btn-primary"><span class="cil-contrast btn-icon mr-2"></span> Kembali</button></a>
            <button type="button" class="btn btn-primary"><span class="cil-contrast btn-icon mr-2"></span> Tambah Produk</button>
        </div>

        @if (!empty($header['error_code']))
            <div class="col-12 col-md-12 mt-3">
                <div class="alert alert-danger" role="alert">
                    {{ $header['error_code'] }} | {{ $header['messages'] }}
                </div>
            </div>
        @endif

        <div class="col-12 col-md-12 mt-2">
            <table class="table table-striped table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Product</th>
                        <th scope="col">Image</th>
                        <th scope="col">SKU</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Stok</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($products as $no => $product)
                        <tr>
                            <th scope="row">{{ $no+1 }}</th>
                            <td>{{ $product['name'] }}</td>
                            <td><img src="{{ $product['image_url'] }}" style="width:70px"></td>
                            <td>{{ $product['sku'] }}</td>
                            <td>{{ $product['price'] }}</td>
                            <td>{{ $product['stock'] }}</td>
                            <td>Active</td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7" class="text-center">Produk Tidak Tersedia untuk tokotertentu</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>

@endsection
