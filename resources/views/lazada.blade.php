@extends('layouts.app', ['class' => 'bg-default'])

@section('content')

    <h1 class="text-center mt-5">Data Product - Lazada</h1>
    <div class="row mt-5">
        <div class="col-12 col-md-12">
            <a href="{{route('/')}}"><button type="button" class="btn btn-primary"><span class="cil-contrast btn-icon mr-2"></span> Kembali</button></a>
            <button type="button" class="btn btn-primary"><span class="cil-contrast btn-icon mr-2"></span> Tambah Produk</button>
        </div>
        <div class="col-12 col-md-12 mt-2">
            <table class="table table-striped table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Product</th>
                        <th scope="col">Image</th>
                        <th scope="col">SKU</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Stok</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $no => $product)
                        <tr>
                            <th scope="row">{{ $no+1 }}</th>
                            <td>{{ $product['attributes']['name'] }}</td>
                            <td><img src="{{ $product['images'][0] }}" style="width:70px"></td>
                            <td>{{ $product['skus'][0]['SellerSku'] }}</td>
                            <td>{{ $product['skus'][0]['price'] }}</td>
                            <td>{{ $product['skus'][0]['quantity'] }}</td>
                            <td>{{ $product['skus'][0]['Status'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
