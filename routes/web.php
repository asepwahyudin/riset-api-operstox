<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("home");
});

Route::get('/', array('as' => '/', 'uses' => 'HomeController@index'));

Route::get('/tokopedia', array('as' => 'tokopedia', 'uses' => 'TokopediaController@index'));
Route::get('/tokopedia/callback', array('as' => 'tokopedia.callback', 'uses' => 'TokopediaController@callback'));

Route::get('/bukalapak', array('as' => 'bukalapak', 'uses' => 'BukalapakController@index'));

Route::get('/blibli', array('as' => 'blibli', 'uses' => 'BlibliController@index'));

Route::get('/lazada', array('as' => 'lazada', 'uses' => 'LazadaController@index'));
Route::get('/lazada/callback', array('as' => 'lazada.callback', 'uses' => 'LazadaController@callback'));
