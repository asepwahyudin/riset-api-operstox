<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LazadaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/LazopClient.php';
        require_once app_path() . '/Helpers/LazopLogger.php';
        require_once app_path() . '/Helpers/LazopRequest.php';
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
