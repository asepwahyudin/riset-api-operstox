<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BlibliController extends Controller
{
    public function index(){
        $host =  "https://api.blibli.com/v2/";
        $url = "proxy/mta/api/businesspartner/v2/product/getProductList";

        $header = [
            "Authorization" => "Basic bXRhLWFwaS1vcGVyc3RveGluZG9uZXNpLTc0YzBhOm10YS1hcGktY3lUQVN0Y3JxRkQyZHVEQVNPN2oxdmk0NkxVZklTWDZiMmlqQXVwYVZXMndyOHJCVU4=",
            "Accept" => "application/json",
            "Content-Type" => "application/json",
            "Api-Seller-Key" => "6FAD14E6794BB0B6342CF5488CB74293E59716144328232D6A72C739337E3C2A",
            "Signature" => "WBy5z2Jxmaead/w8AeEnDIhdqmB9ImlWJmovecAgJec=",
            "Signature-time" => "1627223577218",
        ];

        $requestData = [
            "requestId"=> "f8127be2-1da4-4e65-9280-42137d0d80ed",
            "businessPartnerCode" => "OPX-70001",
            "username" => "asep.wahyudin@mailinator.com",
            "channelId" => "OperstoxIndonesia",
        ];

        $response = Http::withHeaders($header)->post($host . $url, $requestData);
        $results = json_decode($response->body());

        dd($results);
        
    }
}
