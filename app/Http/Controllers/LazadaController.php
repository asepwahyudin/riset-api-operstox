<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserRiset;
use App\Helpers\LazopClient;
use App\Helpers\LazopRequest;

class LazadaController extends Controller
{
    public function index(){
        $check = UserRiset::where('marketplace', 'lazada')->first();
        if (empty($check)) {
            return redirect('https://auth.lazada.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri=https://riset.operstox.com/lazada/callback&client_id=102408');
        } else {
            $c = new LazopClient("https://api.lazada.co.id/rest", "102408", "8Nij9Kp6ms1QwpxOuXSdskMVfa05GABf");
            $request = new LazopRequest('/products/get','GET');
            $request->addApiParam('filter','live');
            $api = json_decode($c->execute($request, $check->access_token), true);
            $data = $api['data'];

            return view('lazada', $data);
        }
    }

    public function callback(){
        if (!empty($_GET['code'])) {
            $code = $_GET['code'];
            $this->saveToken($code);
            return redirect()->route('lazada');
        } else {
            echo "Kode API : Terjadi kesalahan dalam Auth";
        }
    }

    public function saveToken($code){
        $c = new LazopClient("https://auth.lazada.com/rest", "102408", "8Nij9Kp6ms1QwpxOuXSdskMVfa05GABf");
        $request = new LazopRequest("/auth/token/create");
        $request->addApiParam("code", $code);
        $data = json_decode($c->execute($request), true);

        if (!empty($data['access_token'])) {
            $user = new UserRiset;
            $user->access_token = $data['access_token'];
            $user->refresh_token = $data['refresh_token'];
            $user->country = $data['country'];
            $user->refresh_expires_in = $data['refresh_expires_in'];
            $user->account_platform = $data['account_platform'];
            $user->token_type = $code;
            $user->expires_in = $data['expires_in'];
            $user->account = $data['account'];
            $user->marketplace = "lazada";
            $user->save();
        } else {
            echo "Terjadi kesalahan dalam proses pengambilan Token";
        }
    }
}
