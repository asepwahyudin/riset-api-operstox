<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserRiset;

class TokopediaController extends Controller
{
    public function index(){
        $check = UserRiset::where('marketplace', 'tokopedia')->first();
        if (empty($check)) {
            return redirect()->route('tokopedia.callback');
        } else {
            $curl = curl_init();

            $url = "https://fs.tokopedia.net/inventory/v1/fs/15368/product/list?shop_id=4037427&rows=1&start=1&order_by=3";

            curl_setopt_array($curl,
                array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_HTTPGET => true,
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$check->access_token,
                        'Access-Control-Allow-Origin: *'
                    ),
                )
            );

            $response = curl_exec($curl);
            $api = json_decode($response, true);

            if (empty($api)) {
                $data['header']['error_code'] = "403 - Forbidden";
                $data['header']['messages'] = "Anda tidak memiliki akses untuk mengolah data";
                $data['products'] = array();
            } else {
                $data = $api['data'];
                $data['header'] = $api['header'];
            }

            return view('tokopedia', $data);
        }
    }

    public function callback(){

        $url_token =  "https://accounts.tokopedia.com/token?grant_type=client_credentials";

        $client_data = array(
            "grant_type" => "client_credentials",
            "username" => "5345f898b9704d3dbabcfd98509b0c34",
            "password" => "df222c7601b34ac79231d42bee531883"
        );

        $auth = "5345f898b9704d3dbabcfd98509b0c34:df222c7601b34ac79231d42bee531883";

        $curl = curl_init($url_token);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $auth); //Your credentials goes here
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($client_data));

        $curl_response = curl_exec($curl);
        $response = json_decode($curl_response, true);
        curl_close($curl);

        //harus ada pengecekan apakah response access token & expires in !empty, kalo empty. lognya beda.
        if (empty($response['access_token']) || empty($response['expires_in'])) {
            echo "access_token or expires_in from Tokopedia API is empty";
        } else {
            $user = new UserRiset;
            $user->access_token = $response['access_token'];
            $user->token_type = $response['token_type'];
            $user->expires_in = $response['expires_in'];
            $user->marketplace = "tokopedia";
            $user->save();
        }

        return redirect()->route('tokopedia');

    }
}
