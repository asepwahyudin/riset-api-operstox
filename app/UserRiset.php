<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRiset extends Model
{
      /**
       * The table associated with the model.
       *
       * @var string
       */
      protected $table = 'user_riset';

      /**
       * The primary key associated with the table.
       *
       * @var string
       */
      protected $primaryKey = 'id';
}
